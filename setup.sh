#!/usr/bin/env bash

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    echo "Error: ashm bashrc utils not installed" >&2
    exit 1
fi

if [ -z "$XDG_BIN_HOME" ] || ! [ -d "$XDG_BIN_HOME" ]; then

    echo "Error: xdg directories not setup" >&2
    exit
fi

if ! command -v cargo; then

    echo "Error: cargo not installed" >&2
    exit 1
fi


cargo install zellij

script_dir="$(realpath $(dirname ${BASH_SOURCE[0]}))"
zellij_config_home="$HOME/.config/zellij"


# install config
mkdir -p "$zellij_config_home"
rm -f "${zellij_config_home}/config.kdl"
ln -s "${script_dir}/config.kdl" "${zellij_config_home}/config.kdl"


# install aliases
rm -rf "${XDG_BIN_HOME}/alias_zellij.sh"
ln -s "${script_dir}/alias_zellij.sh" "${XDG_BIN_HOME}/alias_zellij.sh"

add_section_to_bashrc "zellij"
source_file_in_bashrc_section "alias_zellij.sh" "zellij"

